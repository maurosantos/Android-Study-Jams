package aprendendodireito.example.com.aprendendodireito;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class direitosHumanos extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_direitos_humanos);
    }

    // Pega o id do texto Humanos
    public void textoHumanos (View view){
        TextView textoHumanos = (TextView) findViewById(R.id.textoHumanos);
        String nomeHumanos = textoHumanos.getText().toString();

        // Pega o nome da String e coloca em EXTRA_TEXT
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:"));
        intent.putExtra(Intent.EXTRA_SUBJECT, "Direitos Humanos. ");
        intent.putExtra(Intent.EXTRA_TEXT, nomeHumanos);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }
}
