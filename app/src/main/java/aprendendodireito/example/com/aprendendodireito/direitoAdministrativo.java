package aprendendodireito.example.com.aprendendodireito;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class direitoAdministrativo extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_direito_administrativo);
    }

    // Pega o id do texto Administrativo
    public void textoAdministrativo (View view){
        TextView textoAdministrativo = (TextView) findViewById(R.id.textoAdministrativo);
        String nomeAdministrativo = textoAdministrativo.getText().toString();

        // Pega o nome da String e coloca em EXTRA_TEXT
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:"));
        intent.putExtra(Intent.EXTRA_SUBJECT, "Direito Administrativo.");
        intent.putExtra(Intent.EXTRA_TEXT, nomeAdministrativo);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

}
