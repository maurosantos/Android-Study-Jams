package aprendendodireito.example.com.aprendendodireito;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

public class Etica extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_etica);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

    }

    // Pega o id do texto Etica
    public void textoEtica (View view){
        TextView textoEtica = (TextView) findViewById(R.id.textoEtica);
        String nomeEtica = textoEtica.getText().toString();

        // Pega o nome da String e coloca em EXTRA_TEXT
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:"));
        intent.putExtra(Intent.EXTRA_SUBJECT, "Ética, Moral e Direito. ");
        intent.putExtra(Intent.EXTRA_TEXT, nomeEtica);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }

    }

}
