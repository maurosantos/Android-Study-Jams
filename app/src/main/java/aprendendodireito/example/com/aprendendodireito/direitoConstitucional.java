package aprendendodireito.example.com.aprendendodireito;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class direitoConstitucional extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_direito_constitucional);
    }

    // Pega o id do texto Constitucional
    public void textoConstitucional (View view){
        TextView textoConstitucional = (TextView) findViewById(R.id.textoConstitucional);
        String nomeConstitucional = textoConstitucional.getText().toString();

        // Pega o nome da String e coloca em EXTRA_TEXT
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:"));
        intent.putExtra(Intent.EXTRA_SUBJECT, "Direito Constitucional.");
        intent.putExtra(Intent.EXTRA_TEXT, nomeConstitucional);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

}
