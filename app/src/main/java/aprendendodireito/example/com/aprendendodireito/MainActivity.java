package aprendendodireito.example.com.aprendendodireito;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void botaoetica(View view){
        Intent intent = new Intent(this, Etica.class);
        startActivity(intent);
    }

    public void botaopenal(View view){
        Intent intent = new Intent(this, direitoPenal.class);
        startActivity(intent);
    }

    public void botaoHumanos(View view){
        Intent intent = new Intent(this, direitosHumanos.class);
        startActivity(intent);
    }

    public void botaoConstitucional(View view){
        Intent intent = new Intent(this, direitoConstitucional.class);
        startActivity(intent);
    }

    public void botaoAdministrativo(View view){
        Intent intent = new Intent(this, direitoAdministrativo.class);
        startActivity(intent);
    }
}
