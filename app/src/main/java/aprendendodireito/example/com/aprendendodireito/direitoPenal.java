package aprendendodireito.example.com.aprendendodireito;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class direitoPenal extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_direito_penal);
    }

    // Pega o id do texto Penal
    public void textoPenal (View view){
        TextView textoPenal = (TextView) findViewById(R.id.textoPenal);
        String nomePenal = textoPenal.getText().toString();

        // Pega o nome da String e coloca em EXTRA_TEXT
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:"));
        intent.putExtra(Intent.EXTRA_SUBJECT, "Direito Penal.");
        intent.putExtra(Intent.EXTRA_TEXT, nomePenal);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }
}
